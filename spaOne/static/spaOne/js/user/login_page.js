$( document ).ready(function()
{
    console.log("login_page.js");

    $( document ).on('click', '#initial_login', function(e)
    {
        $.ajax({
            type: 'get',
            url: '/spa/initial_login/',
            dataType : 'html',
            error: function(data)
            {
                console.log("issue");
            }
        }).done(function (data, textStatus, jqXHR)
        {
            console.log("code: " + jqXHR.status);
            console.log(data);

            $("#login_or_create").empty().html(data);
        });
    });

    $( document ).on('click', '#initial_create', function(e)
    {
        $.ajax({
            type: 'get',
            url: '/spa/initial_create/',
            dataType : 'html',
            error: function(data)
            {
                console.log("issue");
            }
        }).done(function (data, textStatus, jqXHR)
        {
            console.log("code: " + jqXHR.status);
            console.log(data);

            $("#login_or_create").empty().html(data);
        });
    });

    $(".alert-info").fadeTo(1000, 0.5).slideUp(500, function()
    {
        $(".alert-info").slideUp(500);
    });
});
