$( document ).ready(function()
{
    console.log("main_page.js");
    $("#order_value").val(" ");
    $("#order_text input[type=text]").val("");
    $(".btn-group-justified").hide();

    $('.summary').css(
    {
        "left": $("#order_dynamic span:first-of-type").offset().left
    });

    $("#order_dynamic").hide();

    $('.summary').affix(
    {
        offset:
        {
            top: 50
        }
    });

    $('.summary').hide();

    $.ajax({
        type: 'get',
        url: '../get_categories/',
        dataType : 'html',
        error: function(data)
        {
            console.log("issue with get_categories/");
        }
    }).done(function (data, textStatus, jqXHR)
    {
        console.log("code from get_categories/: " + jqXHR.status);
        $('#category_choosing').empty().html(data);
    });

    $( document ).on('change', '#categories', function(e){
        //        console.log( $( "#categories option:selected" ).text() );
        //        console.log( $( "#categories option:selected" ).val() );
        var category = $( "#categories option:selected" ).val();

        $("#order_dynamic").slideDown(500, function()
        {
            $("#order_dynamic").slideDown(500);
        });

        $.ajax({
            type: 'get',
            url: '../get_dishes/' + category,
            dataType : 'html',
            error: function(data)
            {
                console.log("issue with get_dishes/" + category);
            }
        }).done(function (data, textStatus, jqXHR)
        {
            console.log("code from get_dishes/" + category + ": " + jqXHR.status);

            $("#dishes_table").hide().empty().html(data).fadeTo(1000, 1).slideDown(500, function()
            {
                $("#dishes_table").slideDown(500);
            });
        });

        $.ajax({
            type: 'get',
            url: '../get_addons/',
            dataType : 'html',
            error: function(data)
            {
                console.log("issue with get_addons/");
            }
        }).done(function (data, textStatus, jqXHR)
        {
            console.log("code from get_addons/: " + jqXHR.status);

            $("#addons_table").hide().empty().html(data).fadeTo(1000, 1).slideDown(500, function()
            {
                $("#addons_table").slideDown(500);
            });
        });

        $.ajax({
            type: 'get',
            url: '../get_sauces/',
            dataType : 'html',
            error: function(data)
            {
                console.log("issue with get_sauces/");
            }
        }).done(function (data, textStatus, jqXHR)
        {
            console.log("code from get_sauces/: " + jqXHR.status);

            $("#sauces_table").hide().empty().html(data).fadeTo(1000, 1).slideDown(500, function()
            {
                $("#sauces_table").slideDown(500);
            });
        });

        $(".btn-group-justified, .summary, #order_dynamic").fadeTo(1000, 1).slideDown(500, function()
        {
            $(".btn-group-justified, .summary, #order_dynamic").slideDown(500);
        });
    });

    //variables for order
    var ID_dish = null;
    var ID_addon = null;
    var ID_sauce = null;
    var dish_price = 0;
    var addon_price = 0;
    var sauce_price = 0;
    var dish_name = "";
    var addon_name = "";
    var sauce_name = "";

    $( document ).on("change", '.dish', function(e){
        var dish_url = '../get_dish_price/';
        ID_dish = $('input[name=dishRadios]:checked').val();
        console.log("ID_dish: " + ID_dish);
        if( ID_dish != null )
        {
            dish_url += ID_dish;
        }

        $.ajax({
            type: 'get',
            url: dish_url,
            error: function(data)
            {
                console.log("issue with get_dish_price/" + ID_dish);
            }
        }).done(function (data, textStatus, jqXHR)
        {
            console.log("code from get_dish_price/" + ID_dish + ": " + jqXHR.status);

            dish_price = data.dish_price;
            dish_name = data.dish_name;

            $("#order_value").val(parseFloat(dish_price) + parseFloat(addon_price) + parseFloat(sauce_price));
            $("#order_text input, #order_text textarea").val(dish_name + " + " + addon_name + " + " + sauce_name);
        });
    });

    $( document ).on("change", '.addon', function(e) {
        var addon_url = '../get_addon_price/';
        ID_addon = $('input[name=addonRadios]:checked').val();
        console.log("ID_addon: " + ID_addon);

        if( ID_addon != null )
        {
            addon_url += ID_addon;
        }

        $.ajax({
            type: 'get',
            url: addon_url,
            error: function(data)
            {
                console.log("issue with get_addon_price/" + ID_addon);
            }
        }).done(function (data, textStatus, jqXHR)
        {
            console.log("code from get_addon_price/" + ID_addon  + ": " + jqXHR.status);

            addon_price = data.addition_price;
            addon_name = data.addition_name;

            $("#order_value").val(parseFloat(dish_price) + parseFloat(addon_price) + parseFloat(sauce_price));
            $("#order_text input, #order_text textarea").val(dish_name + " + " + addon_name + " + " + sauce_name);
        });
    });

    $( document ).on("change", '.sauce', function(e) {
        var sauce_url = '../get_addon_price/';
        ID_sauce = $('input[name=sauceRadios]:checked').val();
        console.log("ID_sauce: " + ID_sauce);

        if( ID_addon != null )
        {
            sauce_url += ID_sauce;
        }

        $.ajax({
            type: 'get',
            url: sauce_url,
            error: function(data)
            {
                console.log("issue with get_addon_price/" + ID_sauce);
            }
        }).done(function (data, textStatus, jqXHR)
        {
            console.log("code from get_addon_price/" + ID_sauce  + ": " + jqXHR.status);

            sauce_price = data.addition_price;
            sauce_name = data.addition_name;

            $("#order_value").val(parseFloat(dish_price) + parseFloat(addon_price) + parseFloat(sauce_price));
            $("#order_text input, #order_text textarea").val(dish_name + " + " + addon_name + " + " + sauce_name);
        });
    });

    $( document ).on('click', '#clear_button', function(e){
        $('input[name=addonRadios]:checked').prop('checked', false);
        $('input[name=dishRadios]:checked').prop('checked', false);
        $('input[name=sauceRadios]:checked').prop('checked', false);

        $("#order_value").val(" ");
        $("#order_text input[type=text]").val("");
        ID_dish = null;
        ID_addon = null;
        ID_sauce = null;
        dish_price = 0;
        addon_price = 0;
        sauce_price = 0;
        dish_name = "";
        addon_name = "";
        sauce_name = "";

        $('#category_choosing option:eq(0)').prop('selected', true);
        $('#dishes_table, #addons_table, #sauces_table, .btn-group-justified, #order_dynamic, .summary').fadeTo(1000, 0.1).slideUp(500, function()
        {
            $("#dishes_table, #addons_table, #sauces_table, .btn-group-justified, #order_dynamic, .summary").slideUp(500);
        });
    });

    $( document ).on('submit', '#order_form', function(e){
        e.preventDefault();
        console.log("in submitting order");

        var order_url = '../place_order/';

        console.log(order_url);

        if( ID_dish != null )
        {
            order_url += ID_dish + "/";
        }

        if( ID_addon != null )
        {
            order_url += ID_addon + "/";
        }

        if( ID_sauce != null )
        {
            order_url += ID_sauce + "/";
        }

        console.log(order_url);

        $.ajax({
            type: $("#order_form").attr('method'),
            url: order_url,
            data: $("#order_form").serialize(),
            error: function(data, textStatus, jqXHR)
            {
                console.log('error in ../place_order/' + ID_dish + '/' + ID_addon + '/' + ID_sauce + '/');
                console.log(textStatus);
                console.log(jqXHR);
            }
        }).done(function (data, textStatus, jqXHR)
        {
            console.log('code from post ../place_order/' + ID_dish + '/' + ID_addon + ID_sauce + '/:' + jqXHR.status);

            console.log(data);
        });

        return false;
    });
});
