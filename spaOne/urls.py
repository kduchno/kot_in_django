from django.conf.urls import url

from . import views

app_name = 'spaOne'
urlpatterns = [
    url(r'^$', views.LoginView.as_view(), name='login'),
    url(r'^home/$', views.MainView.as_view(), name='home'),
    url(r'^log_in/$', views.log_user_in, name='log_user_in'),
    url(r'^create_account/$', views.create_account, name='create_account'),
    url(r'^log_out/$', views.log_out, name='log_out'),
    url(r'^get_categories/$', views.get_categories, name='get_categories'),
    url(r'^get_dishes/(?P<dish_cat_id>\w+)/$', views.get_dishes_by_category, name='get_dishes_by_category'),
    url(r'^get_addons/$', views.get_addons, name='get_addons'),
    url(r'^get_sauces/$', views.get_sauces, name='get_sauces'),
    url(r'^oneTimeFill/', views.one_time_fill, name='oneTimeFill'),
    url(r'^get_dish_price/$', views.get_dish_price, name='get_dish_price'),
    url(r'^get_dish_price/(?P<ID_dish>\w+)/$', views.get_dish_price, name='get_dish_price'),
    url(r'^get_addon_price/$', views.get_addon_price, name='get_addon_price'),
    url(r'^get_addon_price/(?P<ID_addition>\w+)/$', views.get_addon_price, name='get_addon_price'),
    url(r'^place_order/$', views.place_order, name='place_order'),
    url(r'^place_order/(?P<ID_dish>\w+)/(?P<ID_addition>\w+)/(?P<ID_sauce>\w+)/$', views.place_order, name='place_order')
]
