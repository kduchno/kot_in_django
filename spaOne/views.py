from django.views import generic
from .forms import LoginForm, CreateForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.conf import settings
from django.contrib.staticfiles import finders
from spaOne.models import *
from django.http import JsonResponse
from pathlib import Path


debug = False


# Create your views here.
class LoginView(generic.ListView):
    template_name = 'spaOne/spa_login.html'

    def get_queryset(self):
        return

    def get_context_data(self, **kwargs):
        context = super(LoginView, self).get_context_data(**kwargs)
        context['form_login'] = LoginForm
        context['form_create'] = CreateForm
        return context


# Main view
class MainView(generic.ListView):
    template_name = 'spaOne/spa.html'

    def get_queryset(self):
        return

    # def get_context_data(self, **kwargs):
    #     context = super(MainView, self).get_context_data(**kwargs)
    #     context['form_logout'] = LogoutForm
    #     context['form_order'] = OrderForm
    #     return context


def create_account(request):
    username = request.POST.get('username')
    password = request.POST.get('password')
    email = request.POST.get('email')

    if not username or not password or not email:
        messages.add_message(request, messages.ERROR, 'Error creating account. Password or username or email not provided - or direct access tried')
        return redirect('/spa/')

    username = request.POST['username']
    password = request.POST['password']
    email = request.POST['email']

    print('creating user {}'.format(username))

    # check if already exist
    user = None
    try:
        user = User.objects.get(username=username)
    except User.DoesNotExist as e:
        print('user {} can be created'.format(username))

    if user:
        print('user {} already exist'.format(username))
        messages.add_message(request, messages.ERROR, 'Username already exists. Please provide different data')
    else:
        new_user = User.objects.create_user(username=username, password=password, email=email)

        print("new_user: {}".format(new_user))

        if new_user:
            login(request, new_user)
            return redirect('/spa/home/')
        else:
            messages.add_message(request, messages.ERROR, 'Error creating user {}. Please check input data'.format(username))

    return redirect('/spa/')


def log_user_in(request):

    username = request.POST.get('username')
    password = request.POST.get('password')

    if not username or not password:
        messages.add_message(request, messages.ERROR, 'Error login in. Password or username not provided - or direct access tried')
        return redirect('/spa/')

    username = request.POST['username']
    password = request.POST['password']

    print('authenticating user {} in'.format(username))

    user = authenticate(username=username, password=password)

    if user is not None:
        print('logging user {} in'.format(username))

        login(request, user)

        print('user {} logged in'.format(username))

        return redirect('/spa/home/')
    else:
        print('user {} authentication failed, trying 2nd verification'.format(username))
        user = User.objects.get(username=username)

        if user is not None:
            messages.add_message(request, messages.ERROR, 'Error login in. Password or username wrong')
            return redirect('/spa/')


@login_required
def get_categories(request):
    # print('in get_categories')

    categories = DishCategory.objects.all()
    items_per_category = []
    dishes = None

    for category in categories:
        try:
            dishes = Dishes.objects.filter(dish_cat_ID=category)
        except Dishes.DoesNotExist as e:
            if debug:
                print('Issue getting dishes for category {}'.format(category))

        items_per_category.append(len(dishes))

    for_select = list(zip(categories, items_per_category))
    # print("")
    # print("for_select: {}".format(for_select))
    # print("")

    return render(request, 'spaOne/category_list.html', {'data': for_select})


@login_required
def get_dishes_by_category(request, dish_cat_id):
    # print('in get_dishes_by_category')
    # print('dish_cat_id: {}'.format(dish_cat_id))

    dishes = None
    paths = []

    img_placeholder = finders.find('spaOne/images/{}'.format("no_dish.gif"))

    to_replace = str(Path(__file__).parent)
    to_replace2 = "/static/"

    img_placeholder = img_placeholder.replace(to_replace, "").replace(to_replace2, "")
    print("img_placeholder => {}".format(img_placeholder))

    try:
        dishes = Dishes.objects.filter(dish_cat_ID=dish_cat_id)
    except Dishes.DoesNotExist as e:
        if debug:
            print('Issue getting dishes for category {}'.format(dish_cat_id))

    if dishes:
        for dish in dishes:
            # print("dish: {}".format(dish))

            dish_pic_path = dish.dish_name.replace(" ", "_").lower() + ".jpg"
            print("dish_pic_path => {}".format(dish_pic_path))

            ret = finders.find('spaOne/images/dish_portfolio/fullsize/{}'.format(dish_pic_path))
            print("ret => {}".format(ret))
            print("")

            if ret:
                ret = ret.replace(to_replace, "").replace(to_replace2, "")
                print("ret => {}".format(ret))
                # paths.append(dish_pic_path)
                paths.append(ret)
            else:
                paths.append(img_placeholder)


    # print(paths)
    # print(dishes)

    for_select = list(zip(dishes, paths))
    # print("")
    # print("for_select: {}".format(for_select))
    # print("")

    return render(request, 'spaOne/dishes_list.html', {'data': for_select})


@login_required
def get_addons(request):
    # print('in get_addons')

    addons = Additions.objects.exclude(addition_name__regex=r'sos.*')

    return render(request, 'spaOne/addons_list.html', {'addons': addons})


@login_required
def get_sauces(request):
    # print('in get_addons')

    sauces = Additions.objects.filter(addition_name__regex=r'sos[^\S\n\t]+.*')

    return render(request, 'spaOne/sauces_list.html', {'sauces': sauces})


def one_time_fill(request):
    if not request.user.is_authenticated():
        return redirect('/spa/')
    elif debug and request.user == 'kduchno':
        print('in one_time_fill')

        categoryArray = (
            'cielecina',
            'golona',
            'kaczka',
            'kalmary',
            'krewetki',
            'kuczak',
            'makaron czajniski',
            'makaron risowy',
            'makaron sojowy',
            'owoce morza',
            'rajs',
            'tofu',
            'wieprzek',
            'wolowinka',
            'zupa',
            'ribsy',
            'rybol'
        )

        addonsArray = (
            ['surufka', 1, 0.5, 0x706c616365686f6c646572],
            ['rajs bialy', 1.2, 0.7, 0x706c616365686f6c646572],
            ['rajs smazony', 2.5, 2, 0x706c616365686f6c646572],
            ['marakon czajniski smazony', 4, 3.5, 0x706c616365686f6c646572],
            ['makaron sojowy', 3.7, 3.2, 0x706c616365686f6c646572],
            ['makaron rajsowy', 3.7, 3.2, 0x706c616365686f6c646572],
            ['frytki', 4, 3.5, 0x706c616365686f6c646572],
            ['fryty po wietnamsku', 5.3, 4.3, 0x706c616365686f6c646572],
            ['quartery ziemniaczane smazone', 4.8, 4.3, 0x706c616365686f6c646572],
            ['quartery ziemniaczane po wietnamsku', 5.5, 5, 0x706c616365686f6c646572],
            ['ballsy ziemniaczane smazone', 4.8, 4.3, 0x706c616365686f6c646572],
            ['rajs gotowany z warzywami w sosie', 6, 5.5, 0x706c616365686f6c646572]
        )

        dishArray = (
            ['Kuczak Bac Bo', 10.3],
            ['Kuczak w sosie cytrynowym', 12.8],
            ['Kuczak Gonbao', 10.3],
            ['Kuczak z grilla na otro', 10.7],
            ['Kuczak Viet Nam', 10.3],
            ['Kuczak orientalny ze swiezymi warzywami', 10.3],
            ['Kuczak slodko-kwasny', 10.3],
            ['Kuczak po czajnisku', 10.3],
            ['Kuczak po indyjsku', 10.3],
            ['Kuczak po tajlandzku', 10.3],
            ['Kuczak z ananasami', 10.3],
            ['Kuczak pieczony na otro', 10.3],
            ['Kuczak pieczony w sezamie', 10.5],
            ['Kuczak w sosie grzybowym', 10.3],
            ['Kuczak na chrupko CimCiaCho', 10.3],
            ['Kuczak w pieciu smakach', 10.3],
            ['Kuczak w sosie pikantnym', 10.3],
            ['Kuczak w sosie curry', 10.3],
            ['(Kuczak) Filet z kuczaka w sosie grillowym', 10.5],
            ['(Kuczak) Filet z kuczaka po tajlandzku', 10.5],
            ['(Kuczak) Filet z kuczaka z warzywami', 10.5],
            ['(Kuczak) Filet z kuczaka w cieście', 10.5],
            ['(Kuczak) Udko z kuczaka po czajnisku', 10.3],
            ['(Kuczak) Udko z kuczaka po hanoisku', 10.3],
            ['(Kuczak) Pieczone udko w sosie grillowym', 10.3],
            ['Wieprzek z grilla na otro', 10.7],
            ['Wieprzek z grilla na otro', 10.7],
            ['Wieprzek orientalny ze swiezymi warzywami', 10.3],
            ['Wieprzek rozmaitosci', 10.3],
            ['Wieprzek slodko-kwasny', 10.3],
            ['Wieprzek pieczony po hanoisku', 10.3],
            ['Wieprzek po tajlandzku', 10.3],
            ['Wieprzek po chińsku', 10.3],
            ['Wieprzek pieczony w sezamie', 10.5],
            ['Wieprzek z ananasem', 10.3],
            ['Wieprzek w pięciu smakach', 10.3],
            ['Wieprzek w sosie pikantnym', 10.3],
            ['Ribsy orientalne z warzywami', 10.3],
            ['Ribsy słodko-kwaśne', 10.3],
            ['Ribsy na ostro', 10.3],
            ['Cielecina orientalna ze swiezymi warzywami', 15],
            ['Cielecina slodko-kwasna', 15],
            ['Cielecina pieczona w sezamie', 15.2],
            ['Cielecina na ostro po malajsku w panierce', 15],
            ['Cielecina specjalna po chińsku w panierce', 15],
            ['Cielecina po tajlandzku w panierce', 15],
            ['Cielecina w pięciu smakach', 15],
            ['Cielecina w sosie curry', 15],
            ['Cielecina w sosie pikantnym', 15],
            ['Wolowinka z fasolką szparagowa', 11.3],
            ['Wolowinka z grilla na ostro', 11.7],
            ['Wolowinka na ostro', 11.5],
            ['Wolowinka rozmaitosci', 11.5],
            ['Wolowinka orientalna ze świeżymi warzywami', 11.5],
            ['Wolowinka w sosie curry', 11.5],
            ['Wolowinka pieczona w sezamie', 11.7],
            ['Wolowinka po tajlandzku', 11.5],
            ['Wolowinka po chińsku', 11.5],
            ['Wolowinka w pięciu smakach', 11.5],
            ['Wolowinka z ananasami', 11.5],
            ['Kaczka pekinska', 16.3],
            ['Kaczka po chinsku', 16.3],
            ['Kaczka po tajlandzku', 16.3],
            ['Golona z rozmaitosciami', 11],
            ['Golona w sosie pikantnym', 11],
            ['Golona pieczona w sezamie', 11],
            ['Golona orientalna', 11],
            ['Tofu w sosie pikantnym', 7.8],
            ['Tofu w sosie pomidorowym', 7.8],
            ['Tofu specjalne z szynką wieprzowa', 9.3],
            ['Rajs smażony z kurczakiem', 11],
            ['Rajs smażony z wołowiną', 11],
            ['Rajs smażony z wieprzowiną', 11],
            ['Rajs smażony z krewetkami', 12.5],
            ['Rajs smażony z owocami morza', 12.5],
            ['Rajs smażony z warzywami', 8],
            ['Kalmary słodko-kwaśne', 11.5],
            ['Kalmary na chrupko', 11.5],
            ['Kalmary w sosie pikantnym', 11.5],
            ['Krewetki z rozmaitościami', 11.5],
            ['Krewetki w sosie pikantnym', 11.5],
            ['Krewetki duże w sezamie', 13.7],
            ['Rybol w cieście', 10.3],
            ['Rybol w cieście w pięciu smakach', 10.3],
            ['Rybol w stylu La Vong', 10.3],
            ['Rybol w sosie słodko-kwaśnym', 10.3],
            ['Zupa z mak. sojowym i kurczakiem', 5.5],
            ['Zupa z mak. ryżowym i kurczakiem', 5.5],
            ['Zupa z mak. sojowym i wołowiną', 5.5],
            ['Zupa Wonton', 5.5],
            ['Zupa z krabem i kurczakiem', 6],
            ['Zupa z węgorzewm i kurczakiem', 6],
            ['Zupa z owocami morza kwaśno-ostra', 6],
            ['Owoce morza w sosie pikantnym', 12],
            ['Owoce morza rozmaitości', 12],
            ['Makaron czajniski z kurczakiem', 11],
            ['Makaron czajniski z cielęciną', 16],
            ['Makaron czajniski z wołowiną', 11],
            ['Makaron czajniski smażony z wieprzowiną', 11],
            ['Makaron czajniski smażony z warzywami', 8],
            ['Makaron risowy z kurczakiem', 11],
            ['Makaron risowy z cielęciną', 16],
            ['Makaron risowy z wołowiną', 11],
            ['Makaron risowy z wieprzowiną', 11],
            ['Makaron risowy z warzywami', 8],
            ['Makaron sojowy z kurczakiem', 11],
            ['Makaron sojowy z cielęciną', 16],
            ['Makaron sojowy z wołowiną', 11],
            ['Makaron sojowy z wieprzowiną', 11],
            ['Makaron sojowy z warzywami', 8],
        )

        for idx, elem in enumerate(addonsArray):
            if debug:
                print('Checking if addon {} exists'.format(elem))

            addon = None
            try:
                addon = Additions.objects.get(addition_name=elem[0])
            except Additions.DoesNotExist as e:
                if debug:
                    print('Addon {} is not in db'.format(elem))

            if not addon:
                if debug:
                    print('Creating {} addon -> {}'.format(idx, elem))
                AddonObject = Additions(addition_name=elem[0], addition_price=elem[1], addition_price_in_set=elem[2])
                AddonObject.save()
            elif debug:
                print('Addon {} exists'.format(elem))

        for idx, elem in enumerate(categoryArray):
            if debug:
                print('Checking if category {} exists'.format(elem))

            category = None
            try:
                category = DishCategory.objects.get(category=elem)
            except DishCategory.DoesNotExist as e:
                if debug:
                    print('Category {} is not in db'.format(elem))

            if not category:
                if debug:
                    print('Creating {} category -> {}'.format(idx, elem))
                CategoryObject = DishCategory(category=elem)
                CategoryObject.save()
            elif debug:
                print('Category {} exists'.format(elem))

        for idx, elem in enumerate(dishArray):
            if debug:
                print('Checking if dish {} exists'.format(elem))
            dish = None
            CategoryObject = None
            try:
                dish = Dishes.objects.get(dish_name=elem[0])
            except Dishes.DoesNotExist as e:
                if debug:
                    print('Dish {} is not in db'.format(elem))

            if not dish:
                if debug:
                    print('Creating {} dish -> {}'.format(idx, elem))

                dish_cat = elem[0].split(' ')
                if debug:
                    print("dishcat elem[0].split(\' \'): {}".format(dish_cat))
                dish_cat = dish_cat[0]
                if debug:
                    print("dishcat dish_cat[0]: {}".format(dish_cat))

                dish_cat = dish_cat.strip('(')
                dish_cat = dish_cat.strip(')')
                dish_cat = dish_cat.strip(' ')
                if debug:
                    print("Dishcat [last]: {}".format(dish_cat))

                if dish_cat.lower() in ('owoce, makaron'):
                    suffix = elem[0].split(' ')[1]
                    suffix = suffix.strip('(')
                    suffix = suffix.strip(')')
                    suffix = suffix.strip(' ')

                    dish_cat += " {}".format(suffix)

                    if debug:
                        print("Dishcat [with sufix]: {}".format(dish_cat))

                try:
                    CategoryObject = DishCategory.objects.get(category=dish_cat.lower())
                except DishCategory.DoesNotExist:
                    if debug:
                        print('No such category')

                if CategoryObject:
                    if debug:
                        print('CategoryObject ID: {}'.format(CategoryObject.ID_dish_category))

                    DishObject = Dishes(dish_name=elem[0], dish_price=elem[1], dish_cat_ID=CategoryObject)
                    DishObject.save()
            elif debug:
                print('Dish {} exists'.format(elem))

        savedCategories = DishCategory.objects.all()
        for elem in savedCategories:
            print('{} - {}'.format(elem.ID_dish_category, elem.category))

        savedAddons = Additions.objects.all()
        for elem in savedAddons:
            print('{} - {} - {} - {}'.format(elem.ID_addition, elem.addition_name, elem.addition_price, elem.addition_price_in_set))

        savedDishes = Dishes.objects.all()
        for elem in savedDishes:
            print('{} - {} - {} - {}'.format(elem.ID_dish, elem.dish_name, elem.dish_price, elem.dish_cat_ID.category))

        # return redirect('/spa/')


@login_required
def log_out(request):
    user = request.user.username

    print('logging user {} out'.format(user))
    messages.add_message(request, messages.INFO, 'User {} logged out.'.format(user))

    logout(request)

    print('user {} logged out'.format(user))

    return redirect('/spa/')


@login_required
def get_dish_price(request, ID_dish = None):
    # print('in get_dish_price')

    dish_price = 0

    if ID_dish:
        dish = Dishes.objects.get(ID_dish=ID_dish)
        dish_price = dish.dish_price
        dish_name = dish.dish_name

    response_data = {}
    response_data['dish_price'] = dish_price
    response_data['dish_name'] = dish_name

    return JsonResponse(response_data)


@login_required
def get_addon_price(request, ID_addition = None):
    # print('in get_addon_price')
    addition_price = 0
    addition_name = ""

    if ID_addition:
        addition = Additions.objects.get(ID_addition=ID_addition)
        addition_price = addition.addition_price
        addition_name = addition.addition_name

    response_data = {}
    response_data['addition_price'] = addition_price
    response_data['addition_name'] = addition_name

    print(addition_price)
    return JsonResponse(response_data)


@login_required
def place_order(request, ID_dish = None, ID_addition = None, ID_sauce = None):

    response = {}
    response_admin = {}
    orderEntry = None
    addonEntry = None
    sauceEntry = None
    currentOrderID = None
    currentOrderDate = None
    order_value = 0

    userObject = User.objects.get(id=request.user.id)

    response_admin['user'] = request.user.username
    response_admin['ID_user'] = request.user.id
    response_admin['ID_dish'] = ID_dish
    response_admin['ID_addon'] = ID_addition
    response_admin['ID_sauce'] = ID_sauce

    response['u1'] = response_admin

    if ID_dish:
        dish = Dishes.objects.get(ID_dish=ID_dish)

        response_admin['dish_name'] = dish.dish_name
        response_admin['dish_price'] = dish.dish_price

        order_value += dish.dish_price

        orderEntry = Orders(dish_ID=dish, user_ID=userObject)
        orderEntry.save()

        currentOrderID = Orders.objects.get(ID_order=orderEntry.pk)
        currentOrderDate = currentOrderID.order_date

    if ID_addition:
        addon = Additions.objects.get(ID_addition=ID_addition)

        response_admin['addon_name'] = addon.addition_name
        response_admin['addon_price'] = addon.addition_price

        order_value += addon.addition_price

        addonEntry = OrderToAddition(order_ID=currentOrderID, addition_ID=addon)
        addonEntry.save()

    if ID_sauce:
        sauce = Additions.objects.get(ID_addition=ID_sauce)

        response_admin['sauce_name'] = sauce.addition_name
        response_admin['sauce_price'] = sauce.addition_price

        order_value += sauce.addition_price

        addonEntry = OrderToAddition(order_ID=currentOrderID, addition_ID=sauce)
        addonEntry.save()

    response_admin['total_order_val'] = order_value

    # after calculating total value - modify orderEntry
    orderEntry.order_value = order_value
    orderEntry.save()



    return JsonResponse(response)


