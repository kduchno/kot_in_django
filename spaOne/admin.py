from django.contrib import admin
from spaOne.models import *


# Register your models here.
class DishAdmin(admin.ModelAdmin):
    # ID_dish cannot be change so this will generate error
    # fields = ['ID_dish', 'dish_name', 'dish_price', 'dish_cat_ID']
    # combining fields with list_display can regulate what is changed and what is shown
    fields = ['dish_name', 'dish_price']

    # ID_dish is listed but only for display, for changing it won't be available
    # ID_dish will be a link to data change form
    list_display = ('ID_dish', 'dish_name', 'dish_price', 'dish_cat_ID', 'dish_pic')

admin.site.register(Dishes, DishAdmin)


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('ID_dish_category', 'category')

admin.site.register(DishCategory, CategoryAdmin)


class OrderToAdditionAdminInline(admin.TabularInline):
    model = OrderToAddition
    classes = ['collapse']

    fields = ('order_ID', 'addition_ID',)


class OrdersAdmin(admin.ModelAdmin):
    inlines = (OrderToAdditionAdminInline,)

    list_display = ('ID_order', 'order_date', 'dish_ID', 'user_ID')

admin.site.register(Orders, OrdersAdmin)


class AdditionsAdmin(admin.ModelAdmin):
    inlines = (OrderToAdditionAdminInline,)

    list_display = ('ID_addition', 'addition_name', 'addition_price', 'addition_price_in_set')

admin.site.register(Additions, AdditionsAdmin)


class OrderToAdditionAdmin(admin.ModelAdmin):
    list_display = ('order_ID', 'addition_ID')

admin.site.register(OrderToAddition, OrderToAdditionAdmin)
