from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Additions(models.Model):
    ID_addition = models.AutoField(primary_key=True)
    addition_name = models.CharField(max_length=100)
    addition_price = models.DecimalField(max_digits=5, decimal_places=2)
    addition_price_in_set = models.DecimalField(max_digits=5, decimal_places=2)

    def __str__(self):
        return self.addition_name


class DishCategory(models.Model):
    ID_dish_category = models.AutoField(primary_key=True)
    category = models.CharField(max_length=100)

    def __str__(self):
        return self.category


class Dishes(models.Model):
    ID_dish = models.AutoField(primary_key=True)
    dish_name = models.CharField(max_length=100)
    dish_price = models.DecimalField(max_digits=5, decimal_places=2)
    dish_pic = models.CharField(max_length=150)  # for partial pic path
    dish_cat_ID = models.ForeignKey(DishCategory, db_column='ID_dish_category', on_delete=models.CASCADE)

    def __str__(self):
        return str(self.dish_name)


class Orders(models.Model):
    ID_order = models.AutoField(primary_key=True)
    order_date = models.DateField(auto_now=False, auto_now_add=True)
    dish_ID = models.ForeignKey(Dishes, db_column='ID_dish', on_delete=models.CASCADE)
    user_ID = models.ForeignKey(User, on_delete=models.CASCADE)
    order_value = models.DecimalField(max_digits=5, decimal_places=2, default='0.00')

    def __str__(self):
        return str(self.ID_order)


class OrderToAddition(models.Model):
    order_ID = models.ForeignKey(Orders, db_column='ID_order', on_delete=models.CASCADE)
    addition_ID = models.ForeignKey(Additions, db_column='ID_addition', on_delete=models.CASCADE)

    def __str__(self):
        toRet = "{} & {}".format(self.order_ID, self.addition_ID)
        return str(self.addition_ID)
