# make sure this is at the top if it isn't already
from django import forms
# from .models import Users
from django.contrib.auth.models import User
from spaOne.models import Orders


class LoginForm(forms.ModelForm):
    # terms = forms.BooleanField(label='I agree to terms& bla bla', initial=False)

    class Meta:
        model = User
        fields = ['username', 'password']
        exclude = ['first_name', 'last_name', 'group', 'user_permissions',
                   'is_staff', 'is_superuser', 'last_login', 'date_joined']

        widgets = {
            'username': forms.TextInput(attrs={'placeholder': "email/username", 'class': "form-control"}),
            'password': forms.PasswordInput(attrs={'class': "form-control", 'placeholder': "password"}),
        }


class CreateForm(forms.ModelForm):
    terms = forms.BooleanField(label='I agree to terms& bla bla', initial=False)

    class Meta:
        model = User
        fields = ['username', 'email', 'password']
        exclude = ['first_name', 'last_name', 'group', 'user_permissions',
                   'is_staff', 'is_superuser', 'last_login', 'date_joined']

        widgets = {
            'username': forms.TextInput(attrs={'placeholder': "email/username", 'class': "form-control"}),
            'email': forms.EmailInput(attrs={'placeholder': "email/username", 'class': "form-control"}),
            'password': forms.PasswordInput(attrs={'class': "form-control", 'placeholder': "password"}),
        }


class LogoutForm(forms.ModelForm):
    class Meta:
        model = User
        exclude = ['username', 'password']


class OrderForm(forms.ModelForm):
    class Meta:
        model = Orders
        exclude = ['ID_order', 'order_date', 'dish_ID', 'user_ID']

#